//
//  ViewController.swift
//  WIDGETHAHA
//
//  Created by FAUZIA UMAR on 20/06/20.
//  Copyright © 2020 FAUZIA UMAR. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource {
    
    @IBOutlet weak var namalabel: UILabel!
    
    var dataManager = DataManager()
    var shuffled: [DataStore] = []
    var category: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shuffled = dataManager.data.shuffled()
        category = shuffled[0].category
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WarnaCell", for: indexPath)
        
        namalabel.text = shuffled[0].nameOfThings
        
        switch category {
        case "fruit":
            cell.backgroundColor = UIColor.orange
        case "vegetable":
             cell.backgroundColor = UIColor.green
        case "grains":
             cell.backgroundColor = UIColor.yellow
        case "proteinfood":
             cell.backgroundColor = UIColor.red
        case "dairy":
             cell.backgroundColor = UIColor.blue
        default:
            print("error")
        }
        
        return cell
    }
}

